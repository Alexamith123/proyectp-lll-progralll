﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Logica;
namespace Proyecto3
{
    public partial class AdministradorAlex : Form
    {
        private int id;
        private string codigo;
        private string tipo;
        private int precio;
        private int capasidad;
        Habitaciones_logica logica = new Habitaciones_logica();
        public AdministradorAlex()
        {
            InitializeComponent();

            llenar_tabla_habitaciones();
            esconder_botones();
        }

        public void llenar_tabla_habitaciones()
        {
            logica.llenar_tabla_de_habitaciones_logica(tabla_habitaciones);
        }
        public void esconder_botones()
        {
            label2.Visible = false;
            label3.Visible = false;
            label4.Visible = false;
            label5.Visible = false;
            button4.Visible = false;
            textCodigo.Visible = false;
            comboTipo.Visible = false;
            textPrecio.Visible = false;
            textCapasidad.Visible = false;
        }

        public void mostrar_botones()
        {
            label2.Visible = true;
            label3.Visible = true;
            label4.Visible = true;
            label5.Visible = true;
            button4.Visible = true;
            textCodigo.Visible = true;
            comboTipo.Visible = true;
            textPrecio.Visible = true;
            textCapasidad.Visible = true;
        }
        public void crear_habitacion()
        {
            string codigo = textCodigo.Text;
            int precio = int.Parse(textPrecio.Text);
            int capasidad = int.Parse(textCapasidad.Text);
            string tipo_s = comboTipo.SelectedItem.ToString();
            int tipo = logica.devolver_numero(tipo_s);
            Habitacion habitacion = new Habitacion(codigo, tipo, precio, capasidad);
            MessageBox.Show(logica.crear_habitacion_logica(habitacion));
            llenar_tabla_habitaciones();
            esconder_botones();

        }
        public void eliminar()
        {

            string codigo = tabla_habitaciones.CurrentRow.Cells[1].Value.ToString();
            MessageBox.Show(codigo);
            MessageBox.Show(logica.eliminar_logica(codigo));
            llenar_tabla_habitaciones();
            esconder_botones();
        }
        public void editar_info()
        {
            id = int.Parse(tabla_habitaciones.CurrentRow.Cells[0].Value.ToString());
            textCodigo.Text = tabla_habitaciones.CurrentRow.Cells[1].Value.ToString();
            comboTipo.Text = tabla_habitaciones.CurrentRow.Cells[2].Value.ToString();
            textPrecio.Text = tabla_habitaciones.CurrentRow.Cells[3].Value.ToString();
            textCapasidad.Text = tabla_habitaciones.CurrentRow.Cells[4].Value.ToString();
            mostrar_botones();
            button4.Visible = false;
        }
        public void editar()
        {
            int tipo = logica.devolver_numero(comboTipo.Text);
            MessageBox.Show(logica.editar_logica(id, textCodigo.Text, tipo, textPrecio.Text, textCapasidad.Text));
            llenar_tabla_habitaciones();
            esconder_botones();
        }
        public void limpar_botones()
        {
            textCodigo.Text = "";
            textPrecio.Text = "";
            textCapasidad.Text = "";
        }

        private void Button1_Click_1(object sender, EventArgs e)
        {
            limpar_botones();
            mostrar_botones();
        }

        private void Button4_Click_1(object sender, EventArgs e)
        {
            crear_habitacion();
        }

        private void Button3_Click_1(object sender, EventArgs e)
        {
            eliminar();
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            editar();
        }

        private void Tabla_habitaciones_CellMouseClick_1(object sender, DataGridViewCellMouseEventArgs e)
        {
            editar_info();
        }
    }

}
