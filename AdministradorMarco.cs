﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LogicaProyecto;

namespace Proyecto
{
    public partial class AdministradorMarco : Form
    {
        LogicaAdministrador log = new LogicaAdministrador();
        public AdministradorMarco()
        {
            InitializeComponent();
            llenarTablaCarros();
            comboBox2.DataSource = log.llenarComboIdSucursales();
            comboBox3.DataSource = log.llenarComboIdTipos();
        }

        

        public void llenarTablaCarros()
        {
            tablaCarros.Rows.Clear();
            if (log.totalCarros() > 0)
            {
                String[] arreglo = log.llenarTablaCarros();
                int filas = log.totalCarros();
                int columnas = 6;
                int x = 0;

                // agregar las columnas
                /*for (int i = 0; i < columnas; i++)
                {
                    tabla.Columns.Add("nombre", "Apellido");
                }*/

                // agrega las filas
                tablaCarros.Rows.Add(filas);


                for (int i = 0; i < filas; i++)
                {
                    for (int j = 0; j < columnas; j++)
                    {
                        tablaCarros.Rows[i].Cells[j].Value = arreglo[x];
                        x++;
                    }
                }
            }
        }
       
        
        

       



        

        private void Button10_Click(object sender, EventArgs e)
        {
            String placa = textBox11.Text;
            String modelo = textBox10.Text;
            String anno = textBox9.Text;
            String sede = log.idSede(comboBox2.SelectedItem.ToString());
            string tipo = log.idTipo(comboBox3.SelectedItem.ToString());
            String estado ="False";
            if (comboBox4.SelectedItem.ToString()=="Activo")
            {
                estado = "True";
            }
            if (placa != "" && modelo != "" && anno != "")
            {
                if (log.registrarCarro(placa, modelo, anno, sede, tipo,estado))
                {
                    MessageBox.Show("Registrado Exitosamente");
                }
                else
                {
                    MessageBox.Show("Auto Ya Registrado!");
                }
            }
            else
            {
                MessageBox.Show("Falta Informacion Importante");
            }
            llenarTablaCarros();
        }

        private void Button9_Click(object sender, EventArgs e)
        {
            String placa = textBox11.Text;
            String modelo = textBox10.Text;
            String anno = textBox9.Text;
            String sede = log.idSede(comboBox2.SelectedItem.ToString());
            string tipo = log.idTipo(comboBox3.SelectedItem.ToString());
            String estado = "False";
            if (comboBox4.SelectedItem.ToString() == "Activo")
            {
                estado = "True";
            }
            if (placa != "" && modelo != "" && anno != "")
            {
                if (log.actualizarCarro(placa, modelo, anno, sede, tipo,estado))
                {
                    MessageBox.Show("Actualizado Exitosamente");
                }
                else
                {
                    MessageBox.Show("Error al Actualizar!");
                }
            }
            else
            {
                MessageBox.Show("Falta Informacion Importante");
            }
            llenarTablaCarros();
        }

        private void Button8_Click(object sender, EventArgs e)
        {
            String placa = textBox11.Text;
            if (placa != "")
            {
                if (log.eliminarCarro(placa))
                {
                    MessageBox.Show("Eliminado Exitosamente");
                }
                else
                {
                    MessageBox.Show("Auto no Eliminado!");
                }
            }
            else
            {
                MessageBox.Show("Falta Informacion Importante");
            }
            llenarTablaCarros();
        }

       

        


        private void TextBox11_TextChanged(object sender, EventArgs e)
        {
            String[] cadena = log.infoCarro(textBox11.Text);

            textBox10.Text = cadena[1];//modelo
            textBox9.Text = cadena[2];//año
            String[] sede = log.infoSede(cadena[3]);
            comboBox2.Text = sede[1];//sede
            string[] tipo = log.infoTipo(cadena[4]);
            comboBox3.Text = tipo[1];//tipo
            string estado = "Inactivo";
            if (cadena[5]=="True")
            {
                estado = "Activo";
            }
            comboBox4.Text = estado;
        }


        

        

        

        

        
        private void TabPage2_Click(object sender, EventArgs e)
        {

        }

        private void Button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Button6_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
