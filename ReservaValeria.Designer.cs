﻿namespace _2ProyectoP3
{
    partial class Reserva
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Reserva));
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.correo = new System.Windows.Forms.TextBox();
            this.nombre = new System.Windows.Forms.TextBox();
            this.cedula = new System.Windows.Forms.TextBox();
            this.monto = new System.Windows.Forms.TextBox();
            this.cant = new System.Windows.Forms.NumericUpDown();
            this.tel = new System.Windows.Forms.Label();
            this.telefono = new System.Windows.Forms.TextBox();
            this.cantNinos = new System.Windows.Forms.NumericUpDown();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.montoN = new System.Windows.Forms.TextBox();
            this.total = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.cant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cantNinos)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.label1.Location = new System.Drawing.Point(32, 241);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(112, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Monto a abonar:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.WhiteSmoke;
            this.label2.Location = new System.Drawing.Point(32, 35);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(125, 17);
            this.label2.TabIndex = 1;
            this.label2.Text = "Nombre Completo:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(32, 72);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(0, 17);
            this.label3.TabIndex = 2;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.WhiteSmoke;
            this.label4.Location = new System.Drawing.Point(28, 84);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(129, 17);
            this.label4.TabIndex = 3;
            this.label4.Text = "Correo Electronico:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.WhiteSmoke;
            this.label5.Location = new System.Drawing.Point(88, 124);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(56, 17);
            this.label5.TabIndex = 4;
            this.label5.Text = "Cedula:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.WhiteSmoke;
            this.label6.Location = new System.Drawing.Point(42, 158);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(102, 17);
            this.label6.TabIndex = 5;
            this.label6.Text = "Cant Espacios:";
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.DarkTurquoise;
            this.button2.Location = new System.Drawing.Point(594, 482);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(140, 36);
            this.button2.TabIndex = 7;
            this.button2.Text = "Reservar Tour";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.Button2_Click);
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.Color.Red;
            this.button3.Location = new System.Drawing.Point(704, 3);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(66, 27);
            this.button3.TabIndex = 8;
            this.button3.Text = "Cerrar";
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Click += new System.EventHandler(this.Button3_Click);
            // 
            // correo
            // 
            this.correo.Location = new System.Drawing.Point(163, 84);
            this.correo.Name = "correo";
            this.correo.Size = new System.Drawing.Size(116, 22);
            this.correo.TabIndex = 9;
            // 
            // nombre
            // 
            this.nombre.Location = new System.Drawing.Point(163, 35);
            this.nombre.Name = "nombre";
            this.nombre.Size = new System.Drawing.Size(116, 22);
            this.nombre.TabIndex = 10;
            // 
            // cedula
            // 
            this.cedula.Location = new System.Drawing.Point(163, 119);
            this.cedula.Name = "cedula";
            this.cedula.Size = new System.Drawing.Size(116, 22);
            this.cedula.TabIndex = 11;
            // 
            // monto
            // 
            this.monto.Enabled = false;
            this.monto.Location = new System.Drawing.Point(163, 241);
            this.monto.Name = "monto";
            this.monto.Size = new System.Drawing.Size(116, 22);
            this.monto.TabIndex = 13;
            // 
            // cant
            // 
            this.cant.Location = new System.Drawing.Point(163, 191);
            this.cant.Maximum = new decimal(new int[] {
            40,
            0,
            0,
            0});
            this.cant.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.cant.Name = "cant";
            this.cant.Size = new System.Drawing.Size(116, 22);
            this.cant.TabIndex = 17;
            this.cant.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.cant.ValueChanged += new System.EventHandler(this.Cant_ValueChanged);
            // 
            // tel
            // 
            this.tel.AutoSize = true;
            this.tel.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tel.Location = new System.Drawing.Point(42, 288);
            this.tel.Name = "tel";
            this.tel.Size = new System.Drawing.Size(90, 17);
            this.tel.TabIndex = 18;
            this.tel.Text = "# Telefonico:";
            // 
            // telefono
            // 
            this.telefono.Location = new System.Drawing.Point(163, 285);
            this.telefono.Name = "telefono";
            this.telefono.Size = new System.Drawing.Size(116, 22);
            this.telefono.TabIndex = 19;
            // 
            // cantNinos
            // 
            this.cantNinos.Location = new System.Drawing.Point(339, 191);
            this.cantNinos.Maximum = new decimal(new int[] {
            40,
            0,
            0,
            0});
            this.cantNinos.Name = "cantNinos";
            this.cantNinos.Size = new System.Drawing.Size(116, 22);
            this.cantNinos.TabIndex = 20;
            this.cantNinos.ValueChanged += new System.EventHandler(this.CantNinos_ValueChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.WhiteSmoke;
            this.label7.Location = new System.Drawing.Point(193, 158);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(55, 17);
            this.label7.TabIndex = 21;
            this.label7.Text = "Adultos";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.WhiteSmoke;
            this.label8.Location = new System.Drawing.Point(360, 158);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(44, 17);
            this.label8.TabIndex = 22;
            this.label8.Text = "Niños";
            // 
            // montoN
            // 
            this.montoN.Enabled = false;
            this.montoN.Location = new System.Drawing.Point(342, 241);
            this.montoN.Name = "montoN";
            this.montoN.Size = new System.Drawing.Size(116, 22);
            this.montoN.TabIndex = 23;
            // 
            // total
            // 
            this.total.Enabled = false;
            this.total.Location = new System.Drawing.Point(552, 241);
            this.total.Name = "total";
            this.total.Size = new System.Drawing.Size(116, 22);
            this.total.TabIndex = 24;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Gainsboro;
            this.label10.Location = new System.Drawing.Point(304, 244);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(16, 17);
            this.label10.TabIndex = 26;
            this.label10.Text = "+";
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.DarkTurquoise;
            this.button1.Location = new System.Drawing.Point(471, 241);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 25);
            this.button1.TabIndex = 27;
            this.button1.Text = "Total:";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.Button1_Click);
            // 
            // Reserva
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.MediumSpringGreen;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(782, 554);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.total);
            this.Controls.Add(this.montoN);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.cantNinos);
            this.Controls.Add(this.telefono);
            this.Controls.Add(this.tel);
            this.Controls.Add(this.cant);
            this.Controls.Add(this.monto);
            this.Controls.Add(this.cedula);
            this.Controls.Add(this.nombre);
            this.Controls.Add(this.correo);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "Reserva";
            this.Text = "Reserva";
            ((System.ComponentModel.ISupportInitialize)(this.cant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cantNinos)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.TextBox correo;
        private System.Windows.Forms.TextBox nombre;
        private System.Windows.Forms.TextBox cedula;
        private System.Windows.Forms.TextBox monto;
        private System.Windows.Forms.NumericUpDown cant;
        private System.Windows.Forms.Label tel;
        private System.Windows.Forms.TextBox telefono;
        private System.Windows.Forms.NumericUpDown cantNinos;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox montoN;
        private System.Windows.Forms.TextBox total;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button button1;
    }
}