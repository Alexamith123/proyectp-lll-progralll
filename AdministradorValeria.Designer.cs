﻿using Datos;

namespace _2ProyectoP3
{
    partial class Administrador
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Administrador));
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.CRUDTours = new System.Windows.Forms.TabPage();
            this.button14 = new System.Windows.Forms.Button();
            this.precioNino = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.button8 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.button10 = new System.Windows.Forms.Button();
            this.button11 = new System.Windows.Forms.Button();
            this.button12 = new System.Windows.Forms.Button();
            this.cap = new System.Windows.Forms.NumericUpDown();
            this.nomb = new System.Windows.Forms.TextBox();
            this.desc = new System.Windows.Forms.TextBox();
            this.precioAdul = new System.Windows.Forms.TextBox();
            this.des = new System.Windows.Forms.TextBox();
            this.tipo = new System.Windows.Forms.ComboBox();
            this.fechaT = new System.Windows.Forms.DateTimePicker();
            this.inte = new System.Windows.Forms.RadioButton();
            this.nac = new System.Windows.Forms.RadioButton();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.tablaT = new System.Windows.Forms.DataGridView();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.cmbCategoria = new System.Windows.Forms.ComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txtPrecio = new System.Windows.Forms.TextBox();
            this.btnCrear = new System.Windows.Forms.Button();
            this.btnGuardar = new System.Windows.Forms.Button();
            this.btnBuscar = new System.Windows.Forms.Button();
            this.txtBuscar = new System.Windows.Forms.TextBox();
            this.btnEliminar = new System.Windows.Forms.Button();
            this.txtDescripcion = new System.Windows.Forms.TextBox();
            this.txtNombre = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.dgvArticulos = new System.Windows.Forms.DataGridView();
            this.articuloENLBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.categoriaENLBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.button6 = new System.Windows.Forms.Button();
            this.tabControl1.SuspendLayout();
            this.CRUDTours.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cap)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tablaT)).BeginInit();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvArticulos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.articuloENLBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.categoriaENLBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.CRUDTours);
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Location = new System.Drawing.Point(37, 1);
            this.tabControl1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(731, 564);
            this.tabControl1.TabIndex = 0;
            // 
            // CRUDTours
            // 
            this.CRUDTours.BackColor = System.Drawing.Color.MediumSpringGreen;
            this.CRUDTours.Controls.Add(this.button14);
            this.CRUDTours.Controls.Add(this.precioNino);
            this.CRUDTours.Controls.Add(this.label12);
            this.CRUDTours.Controls.Add(this.button8);
            this.CRUDTours.Controls.Add(this.button9);
            this.CRUDTours.Controls.Add(this.button10);
            this.CRUDTours.Controls.Add(this.button11);
            this.CRUDTours.Controls.Add(this.button12);
            this.CRUDTours.Controls.Add(this.cap);
            this.CRUDTours.Controls.Add(this.nomb);
            this.CRUDTours.Controls.Add(this.desc);
            this.CRUDTours.Controls.Add(this.precioAdul);
            this.CRUDTours.Controls.Add(this.des);
            this.CRUDTours.Controls.Add(this.tipo);
            this.CRUDTours.Controls.Add(this.fechaT);
            this.CRUDTours.Controls.Add(this.inte);
            this.CRUDTours.Controls.Add(this.nac);
            this.CRUDTours.Controls.Add(this.label11);
            this.CRUDTours.Controls.Add(this.label10);
            this.CRUDTours.Controls.Add(this.label9);
            this.CRUDTours.Controls.Add(this.label8);
            this.CRUDTours.Controls.Add(this.label7);
            this.CRUDTours.Controls.Add(this.label6);
            this.CRUDTours.Controls.Add(this.label5);
            this.CRUDTours.Controls.Add(this.tablaT);
            this.CRUDTours.Location = new System.Drawing.Point(4, 25);
            this.CRUDTours.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.CRUDTours.Name = "CRUDTours";
            this.CRUDTours.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.CRUDTours.Size = new System.Drawing.Size(723, 535);
            this.CRUDTours.TabIndex = 0;
            this.CRUDTours.Text = "CRUD Tours";
            // 
            // button14
            // 
            this.button14.BackColor = System.Drawing.Color.DodgerBlue;
            this.button14.Location = new System.Drawing.Point(743, 201);
            this.button14.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button14.Name = "button14";
            this.button14.Size = new System.Drawing.Size(101, 47);
            this.button14.TabIndex = 4;
            this.button14.Text = "Reportes";
            this.button14.UseVisualStyleBackColor = false;
            // 
            // precioNino
            // 
            this.precioNino.Location = new System.Drawing.Point(564, 350);
            this.precioNino.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.precioNino.Name = "precioNino";
            this.precioNino.Size = new System.Drawing.Size(135, 22);
            this.precioNino.TabIndex = 23;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(459, 350);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(92, 17);
            this.label12.TabIndex = 22;
            this.label12.Text = "Precio Niños:";
            // 
            // button8
            // 
            this.button8.BackColor = System.Drawing.Color.DarkTurquoise;
            this.button8.Location = new System.Drawing.Point(476, 475);
            this.button8.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(107, 34);
            this.button8.TabIndex = 21;
            this.button8.Text = "Seleccionar";
            this.button8.UseVisualStyleBackColor = false;
            this.button8.Click += new System.EventHandler(this.Button8_Click);
            // 
            // button9
            // 
            this.button9.BackColor = System.Drawing.Color.DarkTurquoise;
            this.button9.Location = new System.Drawing.Point(355, 475);
            this.button9.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(104, 34);
            this.button9.TabIndex = 20;
            this.button9.Text = "Refrescar";
            this.button9.UseVisualStyleBackColor = false;
            this.button9.Click += new System.EventHandler(this.Button9_Click);
            // 
            // button10
            // 
            this.button10.BackColor = System.Drawing.Color.DarkTurquoise;
            this.button10.Location = new System.Drawing.Point(5, 475);
            this.button10.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(107, 34);
            this.button10.TabIndex = 19;
            this.button10.Text = "Agregar";
            this.button10.UseVisualStyleBackColor = false;
            this.button10.Click += new System.EventHandler(this.Button10_Click);
            // 
            // button11
            // 
            this.button11.BackColor = System.Drawing.Color.DarkTurquoise;
            this.button11.Location = new System.Drawing.Point(117, 475);
            this.button11.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(107, 34);
            this.button11.TabIndex = 18;
            this.button11.Text = "Modificar";
            this.button11.UseVisualStyleBackColor = false;
            this.button11.Click += new System.EventHandler(this.Button11_Click);
            // 
            // button12
            // 
            this.button12.BackColor = System.Drawing.Color.DarkTurquoise;
            this.button12.Location = new System.Drawing.Point(229, 475);
            this.button12.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(107, 34);
            this.button12.TabIndex = 17;
            this.button12.Text = "Eliminar";
            this.button12.UseVisualStyleBackColor = false;
            this.button12.Click += new System.EventHandler(this.Button12_Click);
            // 
            // cap
            // 
            this.cap.Location = new System.Drawing.Point(564, 217);
            this.cap.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cap.Maximum = new decimal(new int[] {
            40,
            0,
            0,
            0});
            this.cap.Name = "cap";
            this.cap.Size = new System.Drawing.Size(135, 22);
            this.cap.TabIndex = 16;
            this.cap.Value = new decimal(new int[] {
            15,
            0,
            0,
            0});
            // 
            // nomb
            // 
            this.nomb.Location = new System.Drawing.Point(564, 164);
            this.nomb.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.nomb.Name = "nomb";
            this.nomb.Size = new System.Drawing.Size(135, 22);
            this.nomb.TabIndex = 15;
            // 
            // desc
            // 
            this.desc.Location = new System.Drawing.Point(547, 385);
            this.desc.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.desc.Multiline = true;
            this.desc.Name = "desc";
            this.desc.Size = new System.Drawing.Size(165, 78);
            this.desc.TabIndex = 14;
            // 
            // precioAdul
            // 
            this.precioAdul.Location = new System.Drawing.Point(564, 311);
            this.precioAdul.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.precioAdul.Name = "precioAdul";
            this.precioAdul.Size = new System.Drawing.Size(135, 22);
            this.precioAdul.TabIndex = 13;
            // 
            // des
            // 
            this.des.Location = new System.Drawing.Point(564, 113);
            this.des.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.des.Name = "des";
            this.des.Size = new System.Drawing.Size(135, 22);
            this.des.TabIndex = 12;
            // 
            // tipo
            // 
            this.tipo.FormattingEnabled = true;
            this.tipo.Items.AddRange(new object[] {
            "Viaje ",
            "Crucero",
            "Rafting",
            "Canopy",
            "Horse Riding",
            "Caminata",
            "Playa"});
            this.tipo.Location = new System.Drawing.Point(564, 266);
            this.tipo.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tipo.Name = "tipo";
            this.tipo.Size = new System.Drawing.Size(135, 24);
            this.tipo.TabIndex = 11;
            // 
            // fechaT
            // 
            this.fechaT.Location = new System.Drawing.Point(564, 70);
            this.fechaT.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.fechaT.Name = "fechaT";
            this.fechaT.Size = new System.Drawing.Size(135, 22);
            this.fechaT.TabIndex = 10;
            // 
            // inte
            // 
            this.inte.AutoSize = true;
            this.inte.Location = new System.Drawing.Point(589, 18);
            this.inte.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.inte.Name = "inte";
            this.inte.Size = new System.Drawing.Size(110, 21);
            this.inte.TabIndex = 9;
            this.inte.TabStop = true;
            this.inte.Text = "Internacional";
            this.inte.UseVisualStyleBackColor = true;
            // 
            // nac
            // 
            this.nac.AutoSize = true;
            this.nac.Location = new System.Drawing.Point(475, 18);
            this.nac.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.nac.Name = "nac";
            this.nac.Size = new System.Drawing.Size(84, 21);
            this.nac.TabIndex = 8;
            this.nac.TabStop = true;
            this.nac.Text = "Nacional";
            this.nac.UseVisualStyleBackColor = true;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(472, 70);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(86, 17);
            this.label11.TabIndex = 7;
            this.label11.Text = "Fecha Viaje:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(473, 116);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(60, 17);
            this.label10.TabIndex = 6;
            this.label10.Text = "Destino:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(473, 164);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(62, 17);
            this.label9.TabIndex = 5;
            this.label9.Text = "Nombre:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(459, 385);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(86, 17);
            this.label8.TabIndex = 4;
            this.label8.Text = "Descripcion:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(471, 217);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(79, 17);
            this.label7.TabIndex = 3;
            this.label7.Text = "Capacidad:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(471, 266);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(40, 17);
            this.label6.TabIndex = 2;
            this.label6.Text = "Tipo:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(455, 314);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(103, 17);
            this.label5.TabIndex = 1;
            this.label5.Text = "Precio Adultos:";
            // 
            // tablaT
            // 
            this.tablaT.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.tablaT.Location = new System.Drawing.Point(5, 60);
            this.tablaT.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tablaT.Name = "tablaT";
            this.tablaT.RowHeadersWidth = 51;
            this.tablaT.RowTemplate.Height = 24;
            this.tablaT.Size = new System.Drawing.Size(441, 369);
            this.tablaT.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.Color.MediumSpringGreen;
            this.tabPage1.Controls.Add(this.cmbCategoria);
            this.tabPage1.Controls.Add(this.label13);
            this.tabPage1.Controls.Add(this.txtPrecio);
            this.tabPage1.Controls.Add(this.btnCrear);
            this.tabPage1.Controls.Add(this.btnGuardar);
            this.tabPage1.Controls.Add(this.btnBuscar);
            this.tabPage1.Controls.Add(this.txtBuscar);
            this.tabPage1.Controls.Add(this.btnEliminar);
            this.tabPage1.Controls.Add(this.txtDescripcion);
            this.tabPage1.Controls.Add(this.txtNombre);
            this.tabPage1.Controls.Add(this.label4);
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Controls.Add(this.dgvArticulos);
            this.tabPage1.Location = new System.Drawing.Point(4, 25);
            this.tabPage1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tabPage1.Size = new System.Drawing.Size(723, 535);
            this.tabPage1.TabIndex = 1;
            this.tabPage1.Text = "CRUD Articulos";
            this.tabPage1.Enter += new System.EventHandler(this.TabPage1_Enter);
            // 
            // cmbCategoria
            // 
            this.cmbCategoria.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbCategoria.FormattingEnabled = true;
            this.cmbCategoria.Location = new System.Drawing.Point(551, 239);
            this.cmbCategoria.Margin = new System.Windows.Forms.Padding(4);
            this.cmbCategoria.Name = "cmbCategoria";
            this.cmbCategoria.Size = new System.Drawing.Size(149, 24);
            this.cmbCategoria.TabIndex = 14;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(95, 36);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(48, 17);
            this.label13.TabIndex = 13;
            this.label13.Text = "Filtrar:";
            // 
            // txtPrecio
            // 
            this.txtPrecio.Location = new System.Drawing.Point(585, 191);
            this.txtPrecio.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtPrecio.Name = "txtPrecio";
            this.txtPrecio.Size = new System.Drawing.Size(115, 22);
            this.txtPrecio.TabIndex = 12;
            // 
            // btnCrear
            // 
            this.btnCrear.Location = new System.Drawing.Point(551, 331);
            this.btnCrear.Margin = new System.Windows.Forms.Padding(4);
            this.btnCrear.Name = "btnCrear";
            this.btnCrear.Size = new System.Drawing.Size(100, 28);
            this.btnCrear.TabIndex = 11;
            this.btnCrear.Text = "Crear";
            this.btnCrear.UseVisualStyleBackColor = true;
            this.btnCrear.Click += new System.EventHandler(this.BtnCrear_Click);
            // 
            // btnGuardar
            // 
            this.btnGuardar.Location = new System.Drawing.Point(613, 284);
            this.btnGuardar.Margin = new System.Windows.Forms.Padding(4);
            this.btnGuardar.Name = "btnGuardar";
            this.btnGuardar.Size = new System.Drawing.Size(100, 28);
            this.btnGuardar.TabIndex = 10;
            this.btnGuardar.Text = "Guardar";
            this.btnGuardar.UseVisualStyleBackColor = true;
            this.btnGuardar.Click += new System.EventHandler(this.BtnGuardar_Click);
            // 
            // btnBuscar
            // 
            this.btnBuscar.Location = new System.Drawing.Point(343, 30);
            this.btnBuscar.Margin = new System.Windows.Forms.Padding(4);
            this.btnBuscar.Name = "btnBuscar";
            this.btnBuscar.Size = new System.Drawing.Size(100, 28);
            this.btnBuscar.TabIndex = 9;
            this.btnBuscar.Text = "Buscar";
            this.btnBuscar.UseVisualStyleBackColor = true;
            this.btnBuscar.Click += new System.EventHandler(this.BtnBuscar_Click);
            // 
            // txtBuscar
            // 
            this.txtBuscar.Location = new System.Drawing.Point(147, 32);
            this.txtBuscar.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtBuscar.Name = "txtBuscar";
            this.txtBuscar.Size = new System.Drawing.Size(188, 22);
            this.txtBuscar.TabIndex = 8;
            // 
            // btnEliminar
            // 
            this.btnEliminar.Location = new System.Drawing.Point(483, 284);
            this.btnEliminar.Margin = new System.Windows.Forms.Padding(4);
            this.btnEliminar.Name = "btnEliminar";
            this.btnEliminar.Size = new System.Drawing.Size(100, 28);
            this.btnEliminar.TabIndex = 7;
            this.btnEliminar.Text = "Eliminar";
            this.btnEliminar.UseVisualStyleBackColor = true;
            this.btnEliminar.Click += new System.EventHandler(this.Button1_Click);
            // 
            // txtDescripcion
            // 
            this.txtDescripcion.Location = new System.Drawing.Point(565, 92);
            this.txtDescripcion.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtDescripcion.Multiline = true;
            this.txtDescripcion.Name = "txtDescripcion";
            this.txtDescripcion.Size = new System.Drawing.Size(135, 91);
            this.txtDescripcion.TabIndex = 6;
            // 
            // txtNombre
            // 
            this.txtNombre.Location = new System.Drawing.Point(565, 58);
            this.txtNombre.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtNombre.Name = "txtNombre";
            this.txtNombre.Size = new System.Drawing.Size(135, 22);
            this.txtNombre.TabIndex = 5;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(477, 239);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(73, 17);
            this.label4.TabIndex = 4;
            this.label4.Text = "Categoria:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(477, 199);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(101, 17);
            this.label3.TabIndex = 3;
            this.label3.Text = "Precio Unidad:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(477, 97);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(86, 17);
            this.label2.TabIndex = 2;
            this.label2.Text = "Descripcion:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(479, 62);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(66, 17);
            this.label1.TabIndex = 1;
            this.label1.Text = "Nombre: ";
            // 
            // dgvArticulos
            // 
            this.dgvArticulos.AllowUserToAddRows = false;
            this.dgvArticulos.AllowUserToDeleteRows = false;
            this.dgvArticulos.AutoGenerateColumns = false;
            this.dgvArticulos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvArticulos.DataSource = this.articuloENLBindingSource;
            this.dgvArticulos.Location = new System.Drawing.Point(5, 62);
            this.dgvArticulos.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dgvArticulos.Name = "dgvArticulos";
            this.dgvArticulos.ReadOnly = true;
            this.dgvArticulos.RowHeadersWidth = 51;
            this.dgvArticulos.RowTemplate.Height = 24;
            this.dgvArticulos.Size = new System.Drawing.Size(437, 402);
            this.dgvArticulos.TabIndex = 0;
            this.dgvArticulos.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DgvArticulos_CellClick);
            this.dgvArticulos.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DgvArticulos_CellContentClick);
            this.dgvArticulos.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.DgvArticulos_DataError);
            // 
            // button6
            // 
            this.button6.BackColor = System.Drawing.Color.Red;
            this.button6.Location = new System.Drawing.Point(805, 12);
            this.button6.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(69, 32);
            this.button6.TabIndex = 2;
            this.button6.Text = "Cerrar ";
            this.button6.UseVisualStyleBackColor = false;
            this.button6.Click += new System.EventHandler(this.Button6_Click);
            // 
            // Administrador
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.MintCream;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(887, 593);
            this.Controls.Add(this.button6);
            this.Controls.Add(this.tabControl1);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "Administrador";
            this.Text = "Administrador";
            this.tabControl1.ResumeLayout(false);
            this.CRUDTours.ResumeLayout(false);
            this.CRUDTours.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cap)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tablaT)).EndInit();
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvArticulos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.articuloENLBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.categoriaENLBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage CRUDTours;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.DataGridView tablaT;
        private System.Windows.Forms.NumericUpDown cap;
        private System.Windows.Forms.TextBox nomb;
        private System.Windows.Forms.TextBox desc;
        private System.Windows.Forms.TextBox precioAdul;
        private System.Windows.Forms.TextBox des;
        private System.Windows.Forms.ComboBox tipo;
        private System.Windows.Forms.DateTimePicker fechaT;
        private System.Windows.Forms.RadioButton inte;
        private System.Windows.Forms.RadioButton nac;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.Button button12;
        private System.Windows.Forms.TextBox precioNino;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Button button14;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView dgvArticulos;
        private System.Windows.Forms.DataGridViewTextBoxColumn nombreDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn descripcionDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn precioUnidadDataGridViewTextBoxColumn;
        private System.Windows.Forms.BindingSource articuloENLBindingSource;
        private System.Windows.Forms.TextBox txtDescripcion;
        private System.Windows.Forms.TextBox txtNombre;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnEliminar;
        private System.Windows.Forms.Button btnBuscar;
        private System.Windows.Forms.TextBox txtBuscar;
        private System.Windows.Forms.Button btnCrear;
        private System.Windows.Forms.Button btnGuardar;
        private System.Windows.Forms.TextBox txtPrecio;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.ComboBox cmbCategoria;
        private System.Windows.Forms.BindingSource categoriaENLBindingSource;
    }
}