﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LogicaProyecto;

namespace Proyecto3
{
    public partial class AdmAgencia : Form
    {
        public AdmAgencia()
        {
            InitializeComponent();
        }
        LogProyecto log = new LogProyecto();

        private void Button1_Click(object sender, EventArgs e)
        {
            string descripcion = des.Text;
            string nombre = nomb.Text;
            int indice1 = idt.SelectedIndex;
            string idTour = idt.Items[indice1].ToString();
            int indice2 = idh.SelectedIndex;
            string idhotel = idh.Items[indice2].ToString();
            int indice3 = idv.SelectedIndex;
            string idVehi = idv.Items[indice3].ToString();
            string preci = precio.Text;
            string men = log.AgregarPaquete(descripcion, nombre, idTour, idVehi, idhotel, preci);
        }

        private void Button4_Click(object sender, EventArgs e)
        {
            ///modificar
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            precio.Clear();
            des.Clear();
            nomb.Clear();
            idt.ResetText();
            idh.ResetText();
            idv.ResetText();
            DataSet data = log.CargarPaquetes();
            tablaP.AutoGenerateColumns = true;
            tablaP.DataSource = data.Tables[0];
        }

        private void Button3_Click(object sender, EventArgs e)
        {
            int id2 = Convert.ToInt32(tablaP.SelectedRows[0].Cells[0].EditedFormattedValue.ToString());
            string mensaje = log.EliminarPaquete(id2);
            MessageBox.Show(mensaje);
        }

        private void Button7_Click(object sender, EventArgs e)
        {
            ////seleccionar
        }
    }
}
