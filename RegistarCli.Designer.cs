﻿namespace Proyecto3
{
    partial class RegistarCli
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.tel = new System.Windows.Forms.TextBox();
            this.corr = new System.Windows.Forms.TextBox();
            this.con = new System.Windows.Forms.TextBox();
            this.ced = new System.Windows.Forms.TextBox();
            this.nom = new System.Windows.Forms.TextBox();
            this.cod = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.Red;
            this.button1.Location = new System.Drawing.Point(712, 13);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 33);
            this.button1.TabIndex = 0;
            this.button1.Text = "Salir";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.Button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(657, 474);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(92, 38);
            this.button2.TabIndex = 1;
            this.button2.Text = "Registrar";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.Button2_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(51, 63);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(62, 17);
            this.label1.TabIndex = 2;
            this.label1.Text = "Nombre:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(51, 109);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 17);
            this.label2.TabIndex = 3;
            this.label2.Text = "Cedula:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(51, 167);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(85, 17);
            this.label3.TabIndex = 4;
            this.label3.Text = "Contraseña:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(7, 231);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(129, 17);
            this.label4.TabIndex = 5;
            this.label4.Text = "Correo Electronico:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(39, 300);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(68, 17);
            this.label5.TabIndex = 6;
            this.label5.Text = "Telefono:";
            // 
            // tel
            // 
            this.tel.Location = new System.Drawing.Point(142, 295);
            this.tel.Name = "tel";
            this.tel.Size = new System.Drawing.Size(100, 22);
            this.tel.TabIndex = 7;
            // 
            // corr
            // 
            this.corr.Location = new System.Drawing.Point(143, 231);
            this.corr.Name = "corr";
            this.corr.Size = new System.Drawing.Size(100, 22);
            this.corr.TabIndex = 8;
            // 
            // con
            // 
            this.con.Location = new System.Drawing.Point(142, 164);
            this.con.Name = "con";
            this.con.Size = new System.Drawing.Size(112, 22);
            this.con.TabIndex = 9;
            // 
            // ced
            // 
            this.ced.Location = new System.Drawing.Point(205, 109);
            this.ced.Name = "ced";
            this.ced.Size = new System.Drawing.Size(49, 22);
            this.ced.TabIndex = 10;
            // 
            // nom
            // 
            this.nom.Location = new System.Drawing.Point(143, 58);
            this.nom.Name = "nom";
            this.nom.Size = new System.Drawing.Size(111, 22);
            this.nom.TabIndex = 11;
            // 
            // cod
            // 
            this.cod.FormattingEnabled = true;
            this.cod.Items.AddRange(new object[] {
            "CLI"});
            this.cod.Location = new System.Drawing.Point(142, 107);
            this.cod.Name = "cod";
            this.cod.Size = new System.Drawing.Size(57, 24);
            this.cod.TabIndex = 12;
            // 
            // RegistarCli
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 539);
            this.Controls.Add(this.cod);
            this.Controls.Add(this.nom);
            this.Controls.Add(this.ced);
            this.Controls.Add(this.con);
            this.Controls.Add(this.corr);
            this.Controls.Add(this.tel);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Name = "RegistarCli";
            this.Text = "RegistarCli";
            this.Load += new System.EventHandler(this.RegistarCli_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox tel;
        private System.Windows.Forms.TextBox corr;
        private System.Windows.Forms.TextBox con;
        private System.Windows.Forms.TextBox ced;
        private System.Windows.Forms.TextBox nom;
        private System.Windows.Forms.ComboBox cod;
    }
}