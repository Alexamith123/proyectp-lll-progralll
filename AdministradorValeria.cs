﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LogicaProyecto;
using Datos;

namespace _2ProyectoP3
{

    
    public partial class Administrador : Form
    {
        Logica1 log = new Logica1();
        string id1 = "";
        private Articulo_ENL articuloSeleccionado;
        private  Articulo_BOL articuloBOL;

        public Administrador()
        {
            InitializeComponent();
            CargarCategorias();
            articuloBOL = new Articulo_BOL();
        }

        private void CargarCategorias()
        {
            cmbCategoria.ValueMember = "Value";
            cmbCategoria.DisplayMember = "Name";
            cmbCategoria.DataSource = 
                Enum.GetNames(typeof(Categoria_ENL))
                .Zip(
                Enum.GetValues(typeof(Categoria_ENL)).Cast<Categoria_ENL>(),
                (s,i) => new {Name=s, Value=i }
                ).ToList();
        }

        private void Button10_Click(object sender, EventArgs e)
        {
            string tipoV = "";
            if (nac.Checked == true)
                tipoV = "Nacional";
            else if (inte.Checked == true)
                tipoV = "Internacional";

            else {
                MessageBox.Show("Debe de selecionar el viaje!");
             }
            string fech = fechaT.Text;
            string destino = des.Text;
            string nombre = nomb.Text;
            int capacidad = Convert.ToInt32(cap.Value);
            int indice = tipo.SelectedIndex;
            string tipoT = tipo.Items[indice].ToString();
            double precioAdulto = Convert.ToDouble(precioAdul.Text);
            double precioNinos = Convert.ToDouble(precioNino.Text);
            string descripcion = desc.Text;
            string men = log.AgregarTour(tipoV, fech, destino, nombre, capacidad, tipoT, precioAdulto, descripcion,precioNinos);
            MessageBox.Show(men);
            cap.Refresh();
            des.Clear();
            nomb.Clear();
            cap.Refresh();
            tipo.ResetText();
            this.precioAdul.Clear();
            desc.Clear();
        }

        private void Button8_Click(object sender, EventArgs e)
        {
            cap.Refresh();
            des.Clear();
            nomb.Clear();
            cap.Refresh();
            tipo.ResetText();
            precioAdul.Clear();
            desc.Clear();
            id1 = tablaT.SelectedRows[0].Cells[0].EditedFormattedValue.ToString();
            string tipoN = tablaT.SelectedRows[0].Cells[1].EditedFormattedValue.ToString();
            string fecha= tablaT.SelectedRows[0].Cells[2].EditedFormattedValue.ToString();
            string destino = tablaT.SelectedRows[0].Cells[3].EditedFormattedValue.ToString();
            string nombre = tablaT.SelectedRows[0].Cells[4].EditedFormattedValue.ToString();
            string descipcion = tablaT.SelectedRows[0].Cells[5].EditedFormattedValue.ToString();
            string capacidad = tablaT.SelectedRows[0].Cells[6].EditedFormattedValue.ToString();
            string tipotour = tablaT.SelectedRows[0].Cells[7].EditedFormattedValue.ToString();
            string precioT = tablaT.SelectedRows[0].Cells[8].EditedFormattedValue.ToString();
            string precioN = tablaT.SelectedRows[0].Cells[9].EditedFormattedValue.ToString();

            if (tipoN == "Nacional")
                nac.Select();
            else if (tipoN == "Internacional")
                inte.Select();
            fechaT.CustomFormat += fecha;
            des.Text += destino;
            nomb.Text += nombre;
            desc.Text += descipcion;
            cap.Value = Convert.ToDecimal(capacidad);
            tipo.Text+=tipotour;
            precioAdul.Text += precioT;
            precioNino.Text += precioN;



        }

        private void Button9_Click(object sender, EventArgs e)
        {
            cap.Refresh();
            des.Clear();
            nomb.Clear();
            cap.Refresh();
            tipo.ResetText();
            precioAdul.Clear();
            desc.Clear();
            precioNino.Clear();
            DataSet data = log.CargarTours();
            tablaT.AutoGenerateColumns = true;
            tablaT.DataSource = data.Tables[0];
        }

        private void Button11_Click(object sender, EventArgs e)
        {
            string tipoV = "";
            if (nac.Checked == true)
                tipoV = "Nacional";
            else if (inte.Checked == true)
                tipoV = "Internacional";

            else
            {
                MessageBox.Show("Debe de selecionar el viaje!");
            }
            string fech = fechaT.Text;
            string destino = des.Text;
            string nombre = nomb.Text;
            int capacidad = Convert.ToInt32(cap.Value);
            int indice = tipo.SelectedIndex;
            string tipoT = tipo.Items[indice].ToString();
            double precioT = Convert.ToDouble(precioAdul.Text);
            double precioN = Convert.ToDouble(precioNino.Text);
            string descripcion = desc.Text;
            int id = Convert.ToInt32(id1);
            string men = log.ModificarTour(id,tipoV, fech, destino, nombre, capacidad, tipoT, precioT, descripcion, precioN);
            MessageBox.Show(men);
            cap.Refresh();
            des.Clear();
            nomb.Clear();
            cap.Refresh();
            tipo.ResetText();
            precioAdul.Clear();
            precioNino.Clear();
            desc.Clear();
        }

        private void Button12_Click(object sender, EventArgs e)
        {
            int id2 = Convert.ToInt32(tablaT.SelectedRows[0].Cells[0].EditedFormattedValue.ToString());
            string mensaje = log.EliminarTour(id2);
            MessageBox.Show(mensaje);
        }

        private void Button15_Click(object sender, EventArgs e)
        {
            Recepcion re = new Recepcion();
            re.Show();
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            try
            {
                if (articuloSeleccionado.id > 0)
                {
                    DialogResult respuesta = MessageBox.Show(
                        string.Format("Seguro que desea eliminar el articulo {0}",articuloSeleccionado.nombre),
                        "Eliminar",MessageBoxButtons.YesNo);

                    if (respuesta == DialogResult.Yes)
                    {
                        if (articuloBOL.Eliminar(articuloSeleccionado))
                        {
                            MessageBox.Show("Articulo Eliminado.");
                            CargarArticulos();
                            Limpiar();
                        }
                        else
                        {
                            MessageBox.Show("Error.\nNo se pudo eliminar.");
                        }
                    }
                }
            } catch (Exception ex) { MessageBox.Show(ex.Message); }
        }

        private void TabPage1_Enter(object sender, EventArgs e)
        {
            Limpiar();
            CargarArticulos();
        }

        private void Limpiar()
        {
            txtNombre.Clear();
            txtNombre.Focus();
            txtDescripcion.Clear();
            txtPrecio.Clear();
            txtBuscar.Clear();
            articuloSeleccionado = new Articulo_ENL();
            cmbCategoria.SelectedIndex = 0;
        }

        private void CargarArticulos()
        {
            dgvArticulos.DataSource = articuloBOL.ListaArticulos(
                string.IsNullOrWhiteSpace(txtBuscar.Text) ? "" : txtBuscar.Text);
        }

        private void DgvArticulos_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            e.Cancel = true;
        }

        private void BtnBuscar_Click(object sender, EventArgs e)
        {
            CargarArticulos();
        }

        private void MostrarArticulo()
        {
            txtNombre.Text = articuloSeleccionado.nombre;
            txtDescripcion.Text = articuloSeleccionado.descripcion;
            txtPrecio.Text = Convert.ToString(articuloSeleccionado.precio_Unidad);
            cmbCategoria.SelectedValue = (Categoria_ENL)articuloSeleccionado.id_categoria;
        }

        private void BtnGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                Articulo_ENL nuevoArticulo = articuloSeleccionado;

                nuevoArticulo.nombre = txtNombre.Text;
                nuevoArticulo.descripcion = txtDescripcion.Text;
                nuevoArticulo.precio_Unidad = Convert.ToDouble(txtPrecio.Text);
                nuevoArticulo.id_categoria = (int)cmbCategoria.SelectedValue;

                if (articuloBOL.Guardar(nuevoArticulo))
                {
                    MessageBox.Show("Guardado Exitosamente.");
                    CargarArticulos();
                    Limpiar();
                }
                else
                {
                    MessageBox.Show("Intentelo de nuevo.");
                }

            } catch (Exception ex) { MessageBox.Show(ex.Message); }
        }

        private void BtnCrear_Click(object sender, EventArgs e)
        {
            articuloSeleccionado = new Articulo_ENL();
            Limpiar();
        }

        private void DgvArticulos_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            articuloSeleccionado = dgvArticulos.CurrentRow.DataBoundItem as Articulo_ENL;
            MostrarArticulo();
        }

        private void DgvArticulos_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void Button6_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
