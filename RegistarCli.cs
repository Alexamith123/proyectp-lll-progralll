﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LogicaProyecto;

namespace Proyecto3
{
    public partial class RegistarCli : Form
    {
        public RegistarCli()
        {
            InitializeComponent();
        }
        LogProyecto log = new LogProyecto();

        private void Button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            int indice = cod.SelectedIndex;
            string cod1 = cod.Items[indice].ToString();
            string cod3 = ced.Text;
            string codigo = cod1 + "-" + cod3;
            string nombre = nom.Text;
            string contra = con.Text;
            string corre = corr.Text;
            string telef = tel.Text;
            Boolean exi = log.ExisteCodigo(codigo);
            if (exi == true)
                MessageBox.Show("El usuario ya existe\nIntentelo nuevamente!");
            else
            {
                string mens = log.AgregarUsuario(codigo, nombre, contra,corre,telef);
                MessageBox.Show(mens);
                nom.Clear();
                ced.Clear();
                con.Clear();
                corr.Clear();
                tel.Clear();

            }
        }

        private void RegistarCli_Load(object sender, EventArgs e)
        {

        }
    }
}
