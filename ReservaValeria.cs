﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LogicaProyecto;

namespace _2ProyectoP3
{
    public partial class Reserva : Form
    {
        int idR = 0;
        double precioM = 0;
        double precioN = 0;
        int toque = 1;
        int cantToque = 2;
        int cantToque2 = 2;


        Logica2 log = new Logica2();
        public Reserva(int id, double precio, double precioNi)
        {
            idR = id;
            precioM = precio;
            precioN = precioNi;
            InitializeComponent();
            monto.Text = Convert.ToString(precioM / 2);



        }
        public int getFactura()
        {
            var seed = Environment.TickCount;
            var random = new Random(seed);
            return seed;
        }
        public string getDate()
        {
            String fecha = "";
            fecha = DateTime.Now.ToString("dd/MM/yyyy");
            return fecha;
        }


        private void Button3_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            string nomb = nombre.Text;
            string corr = correo.Text;
            string ced = cedula.Text;
            string numt = telefono.Text;
            string monto1 = total.Text;
            int cantA = Convert.ToInt32(cant.Value);
            int cantN = Convert.ToInt32(cantNinos.Value);
            int cantEspacios = cantA + cantN;
            int factura = getFactura();
            if (log.ConsultarEspacios(cantEspacios) == true) { 
                string men = log.ReservarTour(idR, nomb, corr, ced, numt, monto1, getDate(), factura, cantEspacios);
                log.RestarEspaciosTour(cantEspacios, idR);
                MessageBox.Show(men);
                string mens = log.EnviarCorreo(idR, corr, ced, nomb, monto1, cantEspacios, getDate(), factura);
                MessageBox.Show(mens);
            }

            else
            {
                MessageBox.Show("No hay espacios disponibles para esa cantidad de personas");
                nombre.Clear();
                correo.Clear();
                cedula.Clear();
                telefono.Clear();
                total.Clear();
                cant.Refresh();
                cantNinos.ResetText();
                montoN.Clear();
                monto.Clear();
            }
           
           



        }

        private void Cant_ValueChanged(object sender, EventArgs e)
        {
            cantToque = Convert.ToInt32(cant.Value);
            monto.Text = Convert.ToString(precioM/2 * cantToque);
            
            
             
        }

        private void CantNinos_ValueChanged(object sender, EventArgs e)
        {
            cantToque2 = Convert.ToInt32(cantNinos.Value);
            montoN.Text = Convert.ToString(precioN / 2 * cantToque2);
        }


        private void Button1_Click(object sender, EventArgs e)
        {
            double m1 = Convert.ToDouble(monto.Text);
            if (montoN.Text.Equals(""))
            {
                total.Text = Convert.ToString(m1);
            }
            else
            {
                double m2 = Convert.ToDouble(montoN.Text);
                double t = m1 + m2;
                total.Text = Convert.ToString(t);
            }
        }
    }
}
