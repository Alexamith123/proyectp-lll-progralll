﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LogicaProyecto;

namespace _2ProyectoP3
{
    public partial class Recepcion : Form
    {

        Logica2 log = new Logica2();
        public Recepcion()
        {
            InitializeComponent();
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            this.Close();

        }

        private void Button1_Click(object sender, EventArgs e)
        {
            int indice = tipo.SelectedIndex;
            string tipoT = tipo.Items[indice].ToString();
            DataSet data = log.MostrarTours(tipoT);
            if (data == null)
            {
                MessageBox.Show("No Hay Tours Disponibles!!");
            }
            else
            {
                Consultas.AutoGenerateColumns = true;
                Consultas.DataSource = data.Tables[0];
            }

        }

        private void Button3_Click(object sender, EventArgs e)
        {
            int id = Convert.ToInt16(Consultas.SelectedRows[0].Cells[0].EditedFormattedValue.ToString());
            double precio = Convert.ToDouble(Consultas.SelectedRows[0].Cells[5].EditedFormattedValue.ToString());
            double precioN = Convert.ToDouble(Consultas.SelectedRows[0].Cells[6].EditedFormattedValue.ToString());

            Reserva re = new Reserva(id,precio,precioN);
            re.Hide();
            re.Show();
        }
        public string getDate()
        {
            String fecha = "";
            fecha = DateTime.Now.ToString("dd/MM/yyyy");
            return fecha;
        }

        private void TabPage1_Click(object sender, EventArgs e)
        {

        }
    }
}
