﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LogicaProyecto;

namespace Proyecto
{
    public partial class Usuarios : Form
    {
        LogicaUsuarios log = new LogicaUsuarios();
        
      
            public Usuarios()
        {
            InitializeComponent();
            comboHoraInicio.DataSource =llenarComboHoras();
            comboHoraFin.DataSource = llenarComboHoras();
            comboHoraInicio.SelectedIndex = 0;
            comboHoraFin.SelectedIndex = 0;
            
            comboBox1.DataSource =log.llenarComboIdTipos(); 
        }

       
        public string[] llenarComboHoras()
        {
            String[] horas = new string[26];
            for (int i = 0; i < 25; i++)
            {
                if (i < 10)
                {
                    horas[i] = $"0{i}:00:00";
                }
                else
                {
                    horas[i] = $"{i}:00:00";
                }
            }
            return horas;
        }

        private void Usuarios_Load(object sender, EventArgs e)
        {

        }

       

        

       

        private void TextBox4_TextChanged(object sender, EventArgs e)
        {
            
        }

       

        private void BtnBuscar_Click(object sender, EventArgs e)
        {
            string cedula=textBox4.Text;
            string tipo = log.idTipo(comboBox1.SelectedItem.ToString());
            string[] g = dateTimePicker1.Value.ToString().Split(' ');
            string[] k = dateTimePicker2.Value.ToString().Split(' ');
            string[] hi = g[0].Split('/');
            string[] i = k[0].Split('/');


            string fechaInicio = hi[0]+"-"+ hi[1] + "-"+hi[2] + " "+ comboHoraInicio.SelectedItem.ToString();
            string fechaFin = i[0] + "-" + i[1] + "-" + i[2] + " " + comboHoraFin.SelectedItem.ToString();

            
            string[] inicio = comboHoraInicio.SelectedItem.ToString().Split(':');
            string[] fin = comboHoraFin.SelectedItem.ToString().Split(':');
            int r = int.Parse(inicio[0]);
            int f = int.Parse(fin[0]);
            int total = f - r;
            

            DateTime fI = dateTimePicker1.Value.Date;
            DateTime fF = dateTimePicker2.Value.Date;
            TimeSpan horas = fF - fI;
            int h = horas.Days;
            int totalMoney = h * 5000;
            string carro = log.buscarCarro(1,fechaInicio,fechaFin);
            if (carro == "")
            {
                MessageBox.Show("No hay carros para alquilar");
            }
            else
            {
                log.guardarAlquiler(carro,1,"CLI",cedula,fechaInicio,fechaFin,totalMoney);
                MessageBox.Show($"Su Vehiculo es Tipo:{comboBox1.Text},La Placa es:{carro},la Fecha y Hora de inicio es:{fechaInicio},y debe devolverlo antes de:{fechaFin}, el total a pagar es de:{totalMoney}");
            }
        }

        private void Button3_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void ComboHoraInicio_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void DateTimePicker1_ValueChanged(object sender, EventArgs e)
        {

        }

        private void ComboHoraFin_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void ComboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
