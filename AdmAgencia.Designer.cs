﻿namespace Proyecto3
{
    partial class AdmAgencia
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.des = new System.Windows.Forms.TextBox();
            this.precio = new System.Windows.Forms.TextBox();
            this.nomb = new System.Windows.Forms.TextBox();
            this.idh = new System.Windows.Forms.ComboBox();
            this.idt = new System.Windows.Forms.ComboBox();
            this.idv = new System.Windows.Forms.ComboBox();
            this.button5 = new System.Windows.Forms.Button();
            this.tablaP = new System.Windows.Forms.DataGridView();
            this.button7 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.tablaP)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(597, 390);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(86, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Descripcion:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(608, 323);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(52, 17);
            this.label2.TabIndex = 1;
            this.label2.Text = "Precio:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(596, 274);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(87, 17);
            this.label3.TabIndex = 2;
            this.label3.Text = "ID_Vehiculo:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(608, 223);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(66, 17);
            this.label4.TabIndex = 3;
            this.label4.Text = "ID_Hotel:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(608, 165);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(63, 17);
            this.label5.TabIndex = 4;
            this.label5.Text = "ID_Tour:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(608, 105);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(62, 17);
            this.label6.TabIndex = 5;
            this.label6.Text = "Nombre:";
            // 
            // des
            // 
            this.des.Location = new System.Drawing.Point(687, 390);
            this.des.Multiline = true;
            this.des.Name = "des";
            this.des.Size = new System.Drawing.Size(130, 68);
            this.des.TabIndex = 6;
            // 
            // precio
            // 
            this.precio.Location = new System.Drawing.Point(687, 318);
            this.precio.Name = "precio";
            this.precio.Size = new System.Drawing.Size(130, 22);
            this.precio.TabIndex = 7;
            // 
            // nomb
            // 
            this.nomb.Location = new System.Drawing.Point(687, 105);
            this.nomb.Name = "nomb";
            this.nomb.Size = new System.Drawing.Size(130, 22);
            this.nomb.TabIndex = 8;
            // 
            // idh
            // 
            this.idh.FormattingEnabled = true;
            this.idh.Location = new System.Drawing.Point(687, 216);
            this.idh.Name = "idh";
            this.idh.Size = new System.Drawing.Size(130, 24);
            this.idh.TabIndex = 9;
            // 
            // idt
            // 
            this.idt.FormattingEnabled = true;
            this.idt.Location = new System.Drawing.Point(687, 162);
            this.idt.Name = "idt";
            this.idt.Size = new System.Drawing.Size(130, 24);
            this.idt.TabIndex = 10;
            // 
            // idv
            // 
            this.idv.FormattingEnabled = true;
            this.idv.Location = new System.Drawing.Point(687, 271);
            this.idv.Name = "idv";
            this.idv.Size = new System.Drawing.Size(130, 24);
            this.idv.TabIndex = 11;
            // 
            // button5
            // 
            this.button5.BackColor = System.Drawing.Color.Red;
            this.button5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button5.Location = new System.Drawing.Point(802, 2);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(61, 31);
            this.button5.TabIndex = 13;
            this.button5.Text = "Salir";
            this.button5.UseVisualStyleBackColor = false;
            // 
            // tablaP
            // 
            this.tablaP.BackgroundColor = System.Drawing.Color.LightGray;
            this.tablaP.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.tablaP.Location = new System.Drawing.Point(3, 74);
            this.tablaP.Name = "tablaP";
            this.tablaP.RowHeadersWidth = 51;
            this.tablaP.RowTemplate.Height = 24;
            this.tablaP.Size = new System.Drawing.Size(587, 411);
            this.tablaP.TabIndex = 14;
            // 
            // button7
            // 
            this.button7.BackColor = System.Drawing.Color.Teal;
            this.button7.Location = new System.Drawing.Point(522, 531);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(106, 35);
            this.button7.TabIndex = 20;
            this.button7.Text = "Seleccionar";
            this.button7.UseVisualStyleBackColor = false;
            this.button7.Click += new System.EventHandler(this.Button7_Click);
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.Teal;
            this.button2.Location = new System.Drawing.Point(399, 531);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(104, 35);
            this.button2.TabIndex = 19;
            this.button2.Text = "Refrescar";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.Button2_Click);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.Teal;
            this.button1.Location = new System.Drawing.Point(54, 531);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(106, 35);
            this.button1.TabIndex = 18;
            this.button1.Text = "Agregar";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.Button1_Click);
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.Color.Teal;
            this.button4.Location = new System.Drawing.Point(166, 531);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(106, 35);
            this.button4.TabIndex = 17;
            this.button4.Text = "Modificar";
            this.button4.UseVisualStyleBackColor = false;
            this.button4.Click += new System.EventHandler(this.Button4_Click);
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.Color.Teal;
            this.button3.Location = new System.Drawing.Point(278, 531);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(106, 35);
            this.button3.TabIndex = 16;
            this.button3.Text = "Eliminar";
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Click += new System.EventHandler(this.Button3_Click);
            // 
            // AdmAgencia
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Turquoise;
            this.ClientSize = new System.Drawing.Size(875, 607);
            this.Controls.Add(this.button7);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.tablaP);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.idv);
            this.Controls.Add(this.idt);
            this.Controls.Add(this.idh);
            this.Controls.Add(this.nomb);
            this.Controls.Add(this.precio);
            this.Controls.Add(this.des);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "AdmAgencia";
            this.Text = "AdmAgencia";
            ((System.ComponentModel.ISupportInitialize)(this.tablaP)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox des;
        private System.Windows.Forms.TextBox precio;
        private System.Windows.Forms.TextBox nomb;
        private System.Windows.Forms.ComboBox idh;
        private System.Windows.Forms.ComboBox idt;
        private System.Windows.Forms.ComboBox idv;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.DataGridView tablaP;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button3;
    }
}