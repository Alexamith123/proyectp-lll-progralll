﻿using LogicaProyecto;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Logica;

namespace Proyecto3
{
    public partial class Reservacion_final : Form
    {
        Reservacion_logica logica = new Reservacion_logica();
        //Reservacion re = new Reservacion();

        private int identificador;
        public Reservacion_final(int identificador)
        {
            InitializeComponent();

            this.identificador = identificador;
            llenar_combo_servicios();
        }
        Reservacion_logica log = new Reservacion_logica();

        public void reservar()
        {
            DateTime f1 = fecha_llegada.Value;
            DateTime f2 = fecha_salida.Value;
            string nombrePersona = txtName.Text;
            string apellidoPersona = textApellido.Text;
            int cedulaPersona = int.Parse(textCedula.Text);
            DateTime fecha_nacimientoPersona = fecha_salida.Value;
            int tipo_de_persona = 3;
            string servi = comboServicios.SelectedItem.ToString();
            int telefono = int.Parse(textTelefono.Text);
            string correo = textCorreo.Text;
            Persona persona = new Persona(cedulaPersona, nombrePersona, apellidoPersona, fecha_nacimientoPersona, tipo_de_persona);
            MessageBox.Show(logica.Reservacion_de_persona(persona, f1, f2, identificador, servi, telefono, correo));

        }
        public void confirmar()
        {
            string servi = comboServicios.SelectedItem.ToString();
            logica.confi(servi);


        }
        private void Label8_Click(object sender, EventArgs e)
        {
            this.Hide();
            //Reservaion_de_usuario_ya_registrado re = new Reservaion_de_usuario_ya_registrado(identificador);
            //re.Show();
        }


        public void llenar_combo_servicios()
        {
            logica.llenar_combo_ser(comboServicios);
        }

        private void ComboServicios_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void Label12_Click(object sender, EventArgs e)
        {
            this.Hide();
            //Reservacion re = new Reservacion();
            //re.Show();

        }
    }
}
